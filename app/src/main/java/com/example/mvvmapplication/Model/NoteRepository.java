package com.example.mvvmapplication.Model;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class NoteRepository {

    public NoteDao noteDao;
    public LiveData<List<Note>> getAllNotes;

    public NoteRepository(Application application) {
        NoteDatabase database = NoteDatabase.getInstance(application);
        noteDao = database.noteDao();
        getAllNotes = noteDao.getAllNotes();
    }

    public void insert(Note note) {
        new InsertTask(noteDao).execute(note);
    }

    public void update(Note note) {
        new UpdateTask(noteDao).execute(note);
    }

    public void delete(Note note) {
        new DeleteTask(noteDao).execute(note);
    }

    public void deleteAll() {
        new DeleteAllTask(noteDao).execute();
    }

    public LiveData<List<Note>> getAllNote() {
        return getAllNotes;
    }

    public static class InsertTask extends AsyncTask<Note, Void, Void> {

        private NoteDao noteDao;

        public InsertTask(NoteDao noteDao) {
            this.noteDao = noteDao;
        }

        @Override
        protected Void doInBackground(Note... notes) {
            noteDao.insert(notes[0]);
            return null;
        }
    }

    public static class UpdateTask extends AsyncTask<Note, Void, Void> {

        private NoteDao noteDao;

        public UpdateTask(NoteDao noteDao) {
            this.noteDao = noteDao;
        }

        @Override
        protected Void doInBackground(Note... notes) {
            noteDao.update(notes[0]);
            return null;
        }
    }

    public static class DeleteTask extends AsyncTask<Note, Void, Void> {

        private NoteDao noteDao;

        public DeleteTask(NoteDao noteDao) {
            this.noteDao = noteDao;
        }

        @Override
        protected Void doInBackground(Note... notes) {
            noteDao.delete(notes[0]);
            return null;
        }
    }

    public static class DeleteAllTask extends AsyncTask<Void, Void, Void> {

        private NoteDao noteDao;

        public DeleteAllTask(NoteDao noteDao) {
            this.noteDao = noteDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            noteDao.deleteAll();
            return null;
        }
    }

}
