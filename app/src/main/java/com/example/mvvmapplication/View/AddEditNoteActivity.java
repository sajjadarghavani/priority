package com.example.mvvmapplication.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Toast;

import com.example.mvvmapplication.R;

public class AddEditNoteActivity extends AppCompatActivity {
    public static final String EXTRA_ID =" com.example.mvvmapplication.View.EXTRA_ID";
    public static final String EXTRA_Title =" com.example.mvvmapplication.View.EXTRA_Title";
    public static final String EXTRA_Description =" com.example.mvvmapplication.View.EXTRA_Description";
    public static final String EXTRA_Priority =" com.example.mvvmapplication.View.EXTRA_Priority";
    private EditText editTextTitle;
    private EditText editTextDescription;
    private NumberPicker numberPickerPriority;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit_note);
        setupViews();

        numberPickerPriority.setMinValue(1);
        numberPickerPriority.setMaxValue(10);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);

        Intent intent = getIntent();
        if (intent.hasExtra(EXTRA_ID)){
            setTitle("Edit Note");
            editTextTitle.setText(intent.getStringExtra(EXTRA_Title));
            editTextDescription.setText(intent.getStringExtra(EXTRA_Description));
            numberPickerPriority.setValue(intent.getIntExtra(EXTRA_Priority, 1));
        }else {
            setTitle("Add Note");
        }

    }

    private void setupViews() {
        editTextTitle = findViewById(R.id.edit_text_title);
        editTextDescription = findViewById(R.id.edit_text_description);
        numberPickerPriority = findViewById(R.id.number_picker_priority);

    }

    private void setupSave(){
        String title = editTextTitle.getText().toString();
        String description = editTextDescription.getText().toString();
        int priority = numberPickerPriority.getValue();

        if (title.trim().isEmpty() || description.trim().isEmpty()) {
            Toast.makeText(this, "Please insert a title and description", Toast.LENGTH_SHORT).show();
            return;
        }

        Intent data = new Intent();
        data.putExtra(EXTRA_Title, title);
        data.putExtra(EXTRA_Description, description);
        data.putExtra(EXTRA_Priority, priority);

        int id = getIntent().getIntExtra(EXTRA_ID, -1);
        if (id != -1) {
            data.putExtra(EXTRA_ID, id);
        }

        setResult(RESULT_OK, data);
        finish();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.add_note_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save_note:
                setupSave();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
